<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEmailAndAddressToCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('email', 200)->nullable()->after('name')->comment("correo electrónico de la compañia de hasta 200 caracteres");
            $table->string('address',955)->nullable()->after('email')->comment("dirección física de la compañia de hasta 955 caracteres");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->dropColumn("email");
            $table->dropColumn("address");
        });
    }
}
