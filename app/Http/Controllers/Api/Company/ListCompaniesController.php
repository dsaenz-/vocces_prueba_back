<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ListCompaniesController extends Controller
{
    protected $status   = true;
    protected $message   = null;
    protected $resultSet = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        DB::beginTransaction();
        try {
                $this->resultSet = DB::table('companies')->get();
                $this->message  = "¡Datos obtenidos exitosamente! 👌🏻";

                if ($this->resultSet->isEmpty()) {
                    $this->message  = "¡'No se encontraron registros!";
                   }
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            $this->status   = false;
            $this->message   =  "¡¡Ha ocurrido un pequeño error, favor de intentarlo más tarde!";
        }

        return response()->json([
            'resultSet' => $this->resultSet,
            'status'   => $this->status,
            'message'   => $this->message
        ],200);

    }
}
