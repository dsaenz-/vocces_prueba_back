<?php

namespace App\Http\Controllers\Api\Company;

use App\Http\Controllers\Controller;
use App\UseCases\Company\UpdateCompanyStatus;
use Illuminate\Http\Request;
use App\Http\Requests\Company\UpdateCompanyStatusRequest;

class UpdateCompanyStatusController extends Controller
{
    public function __invoke(UpdateCompanyStatusRequest $request, UpdateCompanyStatus $useCase, string $id)
    {
        $company = $useCase->execute($id, $request->status);
        return response($company, 200);
    }
}
