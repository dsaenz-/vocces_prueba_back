<?php
namespace App\UseCases\Company;

use App\Models\Company;

class UpdateCompanyStatus
{
    public function execute(string $companyId, string $status): Company
    {
        $company = Company::findOrFail($companyId);
        $company->status = $status;
        $company->save();
        return $company;
    }
}
