<?php

namespace Tests\Vocces\Company\Routes;

use App\Models\Company;
use Tests\TestCase;

class CompanyListRouteTest extends TestCase
{
    /**
     * @group route
     * @group access-interface
     * @test
     */
    public function getCompanyListRoute()
    {
// Make a GET request to the route
        $response = $this->get('/api/company');

        // Assert that the response has a successful status code
        $response->assertStatus(200);

        // Assert that the response contains the three pre-seeded companies
        $companies = Company::all();
        foreach ($companies as $company) {
            $response->assertJsonFragment([
                'id' => $company->id,
                'name' => $company->name,
                'email' => $company->email,
                'address' => $company->address,
                'status' => $company->status,
            ]);
        }
    }
}
