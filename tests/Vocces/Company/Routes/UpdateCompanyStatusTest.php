<?php

namespace Tests\Vocces\Company\Routes;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;
use App\Models\Company;

class UpdateCompanyStatusTest extends TestCase
{
   // use RefreshDatabase; para que no borre

    public function testUpdateCompanyStatus()
    {
        $faker = \Faker\Factory::create();
        $id = Str::uuid()->toString();
        $testCompany = [
            'id' => $id,
            'name' => $faker->name,
            'email' => $faker->email,
            'address' => $faker->address,
            'status' => 'active',
        ];
        $company = Company::create($testCompany);


       $this->json('PUT', '/api/company/'.$company->id.'/status', [
            'status' => 'active',
        ]);

        $this->assertDatabaseHas('companies', [
            'id' => $company->id,
            'status' => 'active'
        ]);
        Company::where('id', $company->id)->delete();
    }
}
